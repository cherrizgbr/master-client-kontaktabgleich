# master-client-kontaktabgleich #

Dieses Repository ist Teil des Prototyps einer Masterarbeit zum Thema:  
*"Konzeption und prototypische Umsetzung der Integration fachlich getriebener Prozessmodellierung in die Softwareentwicklung unter Verwendung der Business Process Model and Notation 2.0"*  

### Informationen zum Modul ###

Dieses Modul beinhaltet ein Plugin zur Bearbeitung des Prozessschrittes Kontaktabgleich im Prozess Kontaktanfrage. Das Plugin ist nicht selbstständig lauffähig. Es wird durch den Java Client geladen und ausgeführt.
![Plugin Kontaktabgleich](http://www.cherriz.de/master/images/content/plugin_kontaktabgleich.png "Plugin Kontaktabgleich")

### Weiterführende Übersicht ###

Dieses Plugin ist Teil des im Architekturschaubild dargestellten Business Clients. Weiterführende Informationen, Zugang zur Arbeit sowie den anderen Modulen des Prototyps finden Sie auf einer eigenen [Übersichtsseite](www.cherriz.de/master) ([cherriz.de/master](www.cherriz.de/master)).  
![Architekturschaubild Prototyp](http://www.cherriz.de/master/images/content/architektur.png "Architekturschaubild Prototyp")

### Kontakt ###
[www.cherriz.de](www.cherriz.de)  
[www.cherriz.de/master](www.cherriz.de/master)  
[master@cherriz.de](mailto:master@cherriz.de)  


