package de.cherriz.master.businessclient.plugin.kontaktabgleich.internal;

import de.cherriz.master.businessclient.dialog.PluginView;
import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user.User;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;


/**
 * ViewController für die UI des Plugins Kontaktabgleich.
 *
 * @author Frederik Kirsch
 */
public class KontaktabgleichView implements PluginView, EventHandler<ActionEvent> {

    @FXML
    private BorderPane root;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnNew;

    @FXML
    private Button btnDuplicate;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtMail;

    @FXML
    private TextField txtTelefon;

    @FXML
    private TextField txtStrasse;

    @FXML
    private TextField txtOrt;

    @FXML
    private TableView<User> tblKontakte;

    @FXML
    private TableColumn<User, String> clmID;

    @FXML
    private TableColumn<User, String> clmVorname;

    @FXML
    private TableColumn<User, String> clmNachname;

    @FXML
    private TableColumn<User, String> clmMail;

    @FXML
    private TableColumn<User, String> clmTelefon;

    @FXML
    private TableColumn<User, String> clmAdresse;

    private KontaktabgleichViewModel viewModel = null;

    private final ObjectProperty<EventHandler<ActionEvent>> cancelProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<EventHandler<ActionEvent>> newProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<EventHandler<ActionEvent>> duplicateProperty = new SimpleObjectProperty<>();

    @Override
    public void setViewModel(ViewModel model) {
        this.viewModel = model.getCastetModel();

        //TextFelder
        this.txtName.textProperty().bind(Bindings.concat(this.viewModel.vornameProperty()).concat(" ").concat(this.viewModel.nachnameProperty()));
        this.txtMail.textProperty().bind(this.viewModel.mailProperty());
        this.txtTelefon.textProperty().bind(this.viewModel.telefonProperty());
        this.txtStrasse.textProperty().bind(this.viewModel.strasseProperty());
        this.txtOrt.textProperty().bind(Bindings.concat(this.viewModel.plzProperty()).concat(", ").concat(this.viewModel.ortProperty()));

        //Buttons
        this.cancelProperty.set(this);
        this.btnCancel.onActionProperty().bind(this.cancelProperty);
        this.newProperty.set(this);
        this.btnNew.onActionProperty().bind(this.newProperty);
        this.duplicateProperty.set(this);
        this.btnDuplicate.onActionProperty().bind(this.duplicateProperty);
        this.btnDuplicate.disableProperty().bind(this.tblKontakte.getSelectionModel().selectedItemProperty().isNull());

        //Tabelle
        this.clmID.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.clmVorname.setCellValueFactory(new PropertyValueFactory<>("vorname"));
        this.clmNachname.setCellValueFactory(new PropertyValueFactory<>("nachname"));
        this.clmMail.setCellValueFactory(new PropertyValueFactory<>("email"));
        this.clmTelefon.setCellValueFactory(new PropertyValueFactory<>("telefon"));
        this.clmAdresse.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getStrasse() + ", " + param.getValue().getPlz() + " " + param.getValue().getOrt()));
        this.tblKontakte.itemsProperty().bind(this.viewModel.potentialMatchesProperty());
        this.tblKontakte.getSelectionModel().selectedItemProperty().addListener(this.viewModel);
    }

    @Override
    public Pane getRoot() {
        return this.root;
    }

    @Override
    public void handle(ActionEvent event) {
        if (event.getSource().equals(this.btnCancel)) {
            this.viewModel.cancel();
        } else if (event.getSource().equals(this.btnNew)) {
            this.viewModel.newKontakt();
        } else if (event.getSource().equals(this.btnDuplicate)) {
            this.viewModel.duplicateKontakt();
        }
    }

}