package de.cherriz.master.businessclient.plugin.kontaktabgleich.internal;

import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.dialog.basic.ValideEventListener;
import de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user.User;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Das ViewModel für die UI des Plugins Kontaktabgleich.
 * Übersetzt zwischen Modelldaten und UIProperties.
 *
 * @author Frederik Kirsch
 */
public class KontaktabgleichViewModel implements ViewModel, ChangeListener<User> {

    private StringProperty vorname = new SimpleStringProperty();

    private StringProperty nachname = new SimpleStringProperty();

    private StringProperty mail = new SimpleStringProperty();

    private StringProperty telefon = new SimpleStringProperty();

    private StringProperty strasse = new SimpleStringProperty();

    private StringProperty plz = new SimpleStringProperty();

    private StringProperty ort = new SimpleStringProperty();

    private Long selectedUserID = null;

    private ListProperty<User> potentialMatches = new SimpleListProperty<>();

    private List<ValideEventListener<ValideKontaktabgleichEventType>> listener = null;

    /**
     * Konstruktor.
     */
    public KontaktabgleichViewModel() {
        this.listener = new ArrayList<>();
    }

    /**
     * Setzt den Kontakt der in der UI dargestellt werden soll.
     * Der Kontakt ist an die Properties gebunden.
     *
     * @param kontakt Der Kontakt.
     */
    public void setKontakt(User kontakt) {
        this.vorname.set(kontakt.getVorname());
        this.nachname.set(kontakt.getNachname());
        this.mail.set(kontakt.getEmail());
        this.telefon.set(kontakt.getTelefon());
        this.strasse.set(kontakt.getStrasse());
        this.plz.set(kontakt.getPlz().toString());
        this.ort.set(kontakt.getOrt());
    }

    /**
     * Setzt potentielle Uebereinstimmungen für den dargestellten Kontakt.
     * Der Uebereinstimmungen sind an die Properties gebunden.
     *
     * @param potentialMatches Liste potentieller Uebereinstimmungen.
     */
    public void setPotentialMatches(List<User> potentialMatches) {
        ObservableList<User> items = FXCollections.observableList(potentialMatches);
        this.potentialMatches.set(items);
    }

    /**
     * Informiert darüber das die Aktion Neuer Kontakt ausgeloest wurde.
     */
    public void newKontakt() {
        this.listener.forEach(listener -> listener.valideEvent(ValideKontaktabgleichEventType.NEW));
    }

    /**
     * Informiert darüber das die Aktion Abbrechen ausgeloest wurde.
     */
    public void cancel() {
        this.listener.forEach(listener -> listener.valideEvent(ValideKontaktabgleichEventType.CANCEL));
    }

    /**
     * Informiert darüber das die Aktion doppelter Kontakt ausgeloest wurde.
     */
    public void duplicateKontakt() {
        this.listener.forEach(listener -> listener.valideEvent(ValideKontaktabgleichEventType.DUPLICATE));
    }

    /**
     * Liefert die ID des selektierten Users zurück.
     *
     * @return Die UserID.
     */
    public Long getSelectedUserID() {
        return this.selectedUserID;
    }

    /**
     * Property des Vornamens die an den Kontakt gebunden ist.
     *
     * @return Die VornameProperty.
     */
    public StringProperty vornameProperty() {
        return this.vorname;
    }

    /**
     * Property des Nachnamens die an den Kontakt gebunden ist.
     *
     * @return Die NachnameProperty.
     */
    public StringProperty nachnameProperty() {
        return this.nachname;
    }

    /**
     * Property der Mail die an den Kontakt gebunden ist.
     *
     * @return Die MailadresseProperty.
     */
    public StringProperty mailProperty() {
        return this.mail;
    }

    /**
     * Property der Telefonnummer die an den Kontakt gebunden ist.
     *
     * @return Die TelefonnummerProperty.
     */
    public StringProperty telefonProperty() {
        return this.telefon;
    }

    /**
     * Property der Strasse die an den Kontakt gebunden ist.
     *
     * @return Die StrasseProperty.
     */
    public StringProperty strasseProperty() {
        return this.strasse;
    }

    /**
     * Property der PLZ die an den Kontakt gebunden ist.
     *
     * @return Die PLZProperty.
     */
    public StringProperty plzProperty() {
        return this.plz;
    }

    /**
     * Property des Ortes die an den Kontakt gebunden ist.
     *
     * @return Die OrtProperty.
     */
    public StringProperty ortProperty() {
        return this.ort;
    }

    /**
     * Property für potentiell uebereinstimmenden User.
     *
     * @return Property potentiell uebereinstimmender User.
     */
    public ListProperty<User> potentialMatchesProperty() {
        return this.potentialMatches;
    }

    /**
     * Registriert den übergebenen Listener.
     * Dieser wird über Events der UI informiert.
     *
     * @param listener The eventlistener.
     */
    public void addValideEventListener(ValideEventListener<ValideKontaktabgleichEventType> listener) {
        this.listener.add(listener);
    }

    @Override
    public void changed(ObservableValue<? extends User> observable, User oldValue, User newValue) {
        if (newValue != null) {
            this.selectedUserID = newValue.getId();
        } else {
            this.selectedUserID = null;
        }
    }

}