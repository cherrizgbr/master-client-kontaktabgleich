package de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Ein User.
 *
 * @author Frederik Kirsch
 */
@JsonIgnoreProperties({"type", "vertreter"})
public class User {

    private Long id = null;

    private String vorname = null;

    private String nachname = null;

    private String email =  null;

    private String telefon = null;

    private String strasse = null;

    private String ort = null;

    private Integer plz = null;

    /**
     * Leerer Konstruktor für generische Objekterzeugung.
     */
    public User(){}

    /**
     * Konpletter Konstruktor.
     *
     * @param id Die UserID.
     * @param vorname Der Vorname.
     * @param nachname Der Nachname.
     * @param email Die Email.
     * @param telefon Die Telefonnummer.
     * @param strasse Die Strasse.
     * @param ort Der Ort.
     * @param plz DIe PLZ.
     */
    public User(Long id, String vorname, String nachname, String email, String telefon, String strasse, String ort, Integer plz) {
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.email = email;
        this.telefon = telefon;
        this.strasse = strasse;
        this.ort = ort;
        this.plz = plz;
    }

    /**
     * Setzt die ID.
     *
     * @param id Die ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Setzt den Vornamen.
     *
     * @param vorname Der Vorname.
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * Setzt den Nachnamen.
     *
     * @param nachname Der Nachname.
     */
    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    /**
     * Setzt die Email.
     *
     * @param email Die Email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Setzt die Telefonnummer.
     *
     * @param telefon Die Telefonnummer.
     */
    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    /**
     * Setzt die Strasse.
     *
     * @param strasse Die Strasse.
     */
    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    /**
     * Setzt den Ort.
     *
     * @param ort Der Ort.
     */
    public void setOrt(String ort) {
        this.ort = ort;
    }

    /**
     * Setzt die PLZ.
     *
     * @param plz DIe PLZ.
     */
    public void setPlz(Integer plz) {
        this.plz = plz;
    }

    /**
     * @return Die ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * @return Der Vorname.
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * @return Der Nachname.
     */
    public String getNachname() {
        return nachname;
    }

    /**
     * @return Die Email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return Das Telefon.
     */
    public String getTelefon() {
        return telefon;
    }

    /**
     * @return Die Strasse.
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * @return Der Ort.
     */
    public String getOrt() {
        return ort;
    }

    /**
     * @return Die Postleitzahl.
     */
    public Integer getPlz() {
        return plz;
    }

}