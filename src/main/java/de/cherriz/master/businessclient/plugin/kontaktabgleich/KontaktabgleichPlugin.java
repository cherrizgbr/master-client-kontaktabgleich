package de.cherriz.master.businessclient.plugin.kontaktabgleich;

import de.cherriz.master.businessclient.dialog.ViewModel;
import de.cherriz.master.businessclient.dialog.basic.ValideEventListener;
import de.cherriz.master.businessclient.plugin.kontaktabgleich.internal.KontaktabgleichViewModel;
import de.cherriz.master.businessclient.plugin.kontaktabgleich.internal.ValideKontaktabgleichEventType;
import de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user.User;
import de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user.UserService;
import de.cherriz.master.businessclient.shared.Plugin;

import java.util.List;

/**
 * Starterklasse für das Plugin Kontaktabgleich.
 * Das Plugin dient zur Klärung, ob neue Kontakte Duplikate bestehender Kontakte sind oder nicht.
 *
 * @author Frederik Kirsch
 */
public class KontaktabgleichPlugin extends Plugin implements ValideEventListener<ValideKontaktabgleichEventType> {

    private String processKey = null;

    private String taskKey = null;

    private UserService userService = null;

    /**
     * Konstruktor.
     *
     * @param fxml Pfad zur FXML UI Definition.
     * @param name Der Name des Plugins.
     * @param processKey Die ProzessID des unterstuetzten Prozesses.
     * @param taskKey Die TaskID des unterstuetzten Tasks.
     * @param userService Der Service zur Abfrage von Usern.
     */
    public KontaktabgleichPlugin(String fxml, String name, String processKey, String taskKey, UserService userService) {
        super(name, fxml);
        this.processKey = processKey;
        this.taskKey = taskKey;
        this.userService = userService;
    }

    @Override
    public String getProcessDefinitionKey() {
        return this.processKey;
    }

    @Override
    public String getTaskDefinitionKey() {
        return this.taskKey;
    }

    @Override
    protected void startEditing() {
        KontaktabgleichViewModel viewModel = this.getViewModel();
        User kontakt = new User(null, this.getAuftragAttribute("vorname"),
                this.getAuftragAttribute("nachname"),
                this.getAuftragAttribute("email"),
                this.getAuftragAttribute("telefon"),
                this.getAuftragAttribute("strasse"),
                this.getAuftragAttribute("ort"),
                Integer.valueOf(this.getAuftragAttribute("plz")));

        List<User> potentialMatches = this.userService.findPotentialMatches(kontakt);

        viewModel.setKontakt(kontakt);
        viewModel.setPotentialMatches(potentialMatches);
    }

    @Override
    protected ViewModel initViewModel() {
        KontaktabgleichViewModel viewModel = new KontaktabgleichViewModel();
        viewModel.addValideEventListener(this);
        return viewModel;
    }

    @Override
    public void valideEvent(ValideKontaktabgleichEventType type) {
        switch (type) {
            case CANCEL:
                this.cancelEditing();
                break;
            case DUPLICATE:
                KontaktabgleichViewModel viewModel = this.getViewModel();
                this.finishEdition(viewModel.getSelectedUserID().toString());
                break;
            case NEW:
                this.finishEdition("New");
                break;
        }
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
