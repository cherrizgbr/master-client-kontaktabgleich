package de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user;

import java.util.List;

/**
 * Service für den Zugriff auf den REST Service für User.
 *
 * @author Frederik Kirsch
 */
public interface UserService {

    /**
     * Liefert potentielle Uebereinstimmungen mit dem uebergebenen User.
     *
     * @param user User fuer den Uebereinstimmungen gesucht werden sollen.
     * @return Potentielle Uebereinstimmungen.
     */
    List<User> findPotentialMatches(User user);

}