package de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user.internal;

import de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user.User;
import de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user.UserService;
import de.cherriz.master.basic.service.AbstractService;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementierung für den {@link de.cherriz.master.businessclient.plugin.kontaktabgleich.service.user.UserService}.
 *
 * @author Frederik Kirsch
 */
public class UserServiceImpl extends AbstractService implements UserService {

    private String contactPath = null;

    private String namePath = null;

    private String addressPath = null;

    /**
     * Setzt den Pfad für die Funktion Suche Kontakt zu Mail / Telefon.
     *
     * @param contactPath Der Pfad.
     */
    public void setContactPath(String contactPath) {
        this.contactPath = contactPath;
    }

    /**
     * Setzt den Pfad für die Funktion Suche Kontakt zu Name.
     *
     * @param namePath Der Pfad.
     */
    public void setNamePath(String namePath) {
        this.namePath = namePath;
    }

    /**
     * Setzt den Pfad für die Funktion Suche Kontakt zu Adresse.
     *
     * @param addressPath Der Pfad.
     */
    public void setAddressPath(String addressPath) {
        this.addressPath = addressPath;
    }

    @Override
    public List<User> findPotentialMatches(User user) {
        //Request absetzen
        String responseName = this.getTarget(this.namePath.replace("{vorname}", user.getVorname()).replace("{nachname}", user.getNachname()))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);
        String responseAdress = this.getTarget(this.addressPath.replace("{strasse}", user.getStrasse()).replace("{ort}", user.getOrt()).replace("{plz}", user.getPlz().toString()))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);
        String responseContact = this.getTarget(this.contactPath.replace("{mail}", user.getEmail()).replace("{telefon}", user.getTelefon()))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);

        //Ergebnisse auslesen
        List<User> response = new ArrayList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            response.addAll(mapper.readValue(responseName, new TypeReference<List<User>>() {
            }));
            response.addAll(mapper.readValue(responseAdress, new TypeReference<List<User>>() {
            }));
            response.addAll(mapper.readValue(responseContact, new TypeReference<List<User>>() {
            }));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Ergebnisse zusammenfuehren (Duplikate entfernen).
        List<User> result = new ArrayList<>();
        for (User u1 : response) {
            boolean found = false;
            for (User u2 : result) {
                if (u1.getId().equals(u2.getId())) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                result.add(u1);
            }
        }

        return result;
    }

}