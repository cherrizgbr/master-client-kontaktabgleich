package de.cherriz.master.businessclient.plugin.kontaktabgleich.internal;

import de.cherriz.master.businessclient.dialog.basic.ValideEventType;

/**
 * Definiert die verschiedenen Eventtypen für die UI Kontaktabgleich.
 *
 * @author Frederik Kirsch
 */
public enum ValideKontaktabgleichEventType implements ValideEventType {

    /**
     * Abbrechen.
     */
    CANCEL,

    /**
     * Duplikat.
     */
    DUPLICATE,

    /**
     * Neuer Kontakt.
     */
    NEW;

}
